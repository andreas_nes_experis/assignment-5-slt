# Lost in Translation - Create a Sign Language Translator using React
## Description
In this assignment we built an online sign language translator as a single page application using react framework.
The translator is translating words letter by letter.

## Requirements for the sign language translator
This application will have one main feature, to translate english words and short sentences to American sign language, and the program can be divided into three sections:

Login - In this section we've created some functions that lets you log in. You type in your username and hits continue. If your username meets the requirements and doesn't exist in the api, it will automatically be created. The only requirement for the username is having a minimum of 3 characters.

Translation - This is the main section of the application. This is where you can translate words to sign language. After you've logged in, you will automatically be redirected to this page. To translate words you simply type in some input in the input box and click on the right arrow to translate. Your translations will be stored in the api.

Profile - In this section you can see your translation history log, but only your last 10 translations. There are also some options to either log out, or to clear your history. This will only delete your translations from the api and empty your translation history. You'll be given a link to get your first translation.

## Component tree
![Component_tree](/uploads/c7d2f9f0d7e7f41ea3f68cc73cdf89bd/Component_tree.PNG)

## Technologies
* React
* Javascript
* CSS
* HTML
* Vercel
* Railway
* Figma

## Hosting
![Status](https://therealsujitk-vercel-badge.vercel.app/?app=assignment-5)
[**Link to Vercel hosted project**](https://assignment-5-slt.vercel.app/)

## Contributors
* Andreas Nes 
* Jarand Larsen

## License
MIT


# --------- REACT ---------

## Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can't go back!**

If you aren't satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you're on your own.

You don't have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn't feel obligated to use this feature. However we understand that this tool wouldn't be useful if you couldn't customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: [https://facebook.github.io/create-react-app/docs/code-splitting](https://facebook.github.io/create-react-app/docs/code-splitting)

### Analyzing the Bundle Size

This section has moved here: [https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size](https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size)

### Making a Progressive Web App

This section has moved here: [https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app](https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app)

### Advanced Configuration

This section has moved here: [https://facebook.github.io/create-react-app/docs/advanced-configuration](https://facebook.github.io/create-react-app/docs/advanced-configuration)

### Deployment

This section has moved here: [https://facebook.github.io/create-react-app/docs/deployment](https://facebook.github.io/create-react-app/docs/deployment)

### `npm run build` fails to minify

This section has moved here: [https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify](https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify)
