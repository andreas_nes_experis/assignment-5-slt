import { NavLink } from "react-router-dom"
import { useUser } from "../../context/UserContext";

const Navbar = () => {

    const {user} = useUser()

    // Navigation bar that appears at the top of all pages, with links to both Profile and Translate (only if signed in), as well as the website title and logo.
    return (
        <nav class="navbar">
            <img id="nav-logo" src="Logo.png" alt="Logo: smiling robot"/>
            <h1 id="nav-title">Lost in Translation</h1>
            {user !== null &&
            <ul id="navigation">
                <li id="nav-translation"><NavLink to="/translation"><strong>Translate</strong></NavLink></li>
                <li id="nav-profile"><NavLink to="/profile"><strong>Profile</strong></NavLink></li>
            </ul>
            }
        </nav>
    );
}
export default Navbar