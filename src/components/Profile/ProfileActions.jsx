import { translateClearHistory } from "../../api/translate"
import { STORAGE_KEY_USER } from "../../const/storageKeys"
import { useUser } from "../../context/UserContext"
import { storageDelete, storageSave } from "../../utils/storage"
import "../../App.css"

const ProfileActions = () => {
    const {user, setUser} = useUser()

    const handleLogoutClick = () => {
        if (window.confirm("Are you sure you wish to log out?")){
            storageDelete(STORAGE_KEY_USER)
            setUser(null)
            // Could have invoked a logout() function in the parent Profile component instead,
            // But since withAuth so conveniently handles refreshing and redirecting for us,
            // we'll just handle this here so our code doesn't get fragmented.

        }
}

    const handleClearHistoryClick = async () => {
        if (!window.confirm(`Are you sure?\nThis cannot be undone!`)){
            return
        }

        const [clearError] = await translateClearHistory(user.id)
        
        if (clearError !== null){
            return
        }

        const updatedUser = {
            ...user,
            translations: []
        }

        storageSave(STORAGE_KEY_USER, updatedUser)
        setUser(updatedUser)
    }

    return (
        <ul>
            <br />
            <li><button class="button" onClick={ handleClearHistoryClick }>Clear history</button></li>
            <li><button class="button" onClick={ handleLogoutClick }>Log out</button></li>
        </ul>
    )
}
export default ProfileActions