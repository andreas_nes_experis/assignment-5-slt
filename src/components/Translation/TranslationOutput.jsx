import { lettersToSign } from "./TranslationForm"

const TranslationOutput =  () => {
    console.log(lettersToSign)
    const engLettersRegEx = /[a-zA-Z]/ // Only English-language letters can be translated.

    let renderedLetters = lettersToSign.map((letter, index) => {
        return(
            <>
                {engLettersRegEx.test(letter) && // Only retrieve the signs for the characters allowed by the regular expression.
                    <span class="inline" key={index}>
                        <img src= {`individual_signs/${letter.toLowerCase()}.png`} alt = {letter} width="100"/>
                    </span>
                }
                {letter === " " &&
                    <br />
                }
            </>
        )
    })
    return (
        <>
            {renderedLetters}
        </>

    )
}
export default TranslationOutput