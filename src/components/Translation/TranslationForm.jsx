import { useForm } from "react-hook-form"
import TranslationOutput from "./TranslationOutput"
import "../../App.css"

export let lettersToSign = []

const TranslationForm = ({onTranslate}) => {

    const { register, handleSubmit} = useForm()

    //handling the onTranslate
    const onSubmit = ({ translatedText }) => {
        // Split the input into an array of individual characters, and translate.
        lettersToSign = translatedText.split("")
        onTranslate(translatedText) 
    }

    //Handling the translation
    const handleTranslation = e => {
        const {letter, value} = e.target
        console.log({letter} + " " + value)
    }


    return (
        <form onSubmit={handleSubmit(onSubmit) }>
            <fieldset id="translate-input">
                <input type="text" id="input-text" {...register("translatedText", {maxLength: 40})} placeholder="Type here to translate (max. 40 characters)." 
                onChange={handleTranslation} />
                <button id="translate-button" class="button" type="translate">🡆</button>
            </fieldset>
            <fieldset>
                <TranslationOutput />
            </fieldset>
        </form>
    )
}
export default TranslationForm